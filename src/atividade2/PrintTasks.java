/**
 * Exemplo1: Programacao com threads
 * Autor: Marciano da Rocha
 * Ultima modificacao: 01/04/2018
 */
package atividade2;

import java.util.Random;

public class PrintTasks implements Runnable
{

    private final int sleepTime; //tempo de adormecimento aleatorio para a thread
    private final int id;
    private final String taskName; //nome da tarefa
    private final static Random generator = new Random();

    public PrintTasks(String name, int id)
    {
        this.taskName = name;
        this.id = id;

        //Tempo aleatorio entre 0 e 5 segundos
        this.sleepTime = generator.nextInt(1000); //milissegundos
    }

    public void run()
    {
        try
        {
            if (id % 2 != 0)
            {
                System.out.printf("Tarefa: %s dorme por %d ms\n", taskName, sleepTime);
                //Estado de ESPERA SINCRONIZADA
                //Nesse ponto, a thread perde o processador, e permite que
                //outra thread execute            
                Thread.sleep(sleepTime);
                System.out.printf("%s acordou!\n", taskName);
            } else
            {
                System.out.printf("Tarefa par id %d passou controle para outra thread\n", id);
                Thread.yield();
            }
        } catch (InterruptedException ex)
        {
            System.out.printf("%s %s\n", taskName, "terminou de maneira inesperada.\n");
        }
    }

}
