/**
 * Exemplo1: Programacao com threads
 * Autor: Marciano da Rocha
 * Ultima modificacao: 01/04/2018
 */
package atividade2;

public class Exemplo2
{

    public static void main(String[] args)
    {

        System.out.println("Inicio da criacao das threads.");
        Thread t;
        for (int i = 1; i <= 20; i++)
        {
            t = new Thread(new PrintTasks("Thread " + i, i));
            t.start();
        }

        System.out.println("Threads criadas");
    }

}
